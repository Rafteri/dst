package com.rafteri.dst;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rafteri.dst.model.Route;
import com.rafteri.dst.model.SantaMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
class RouteValidationTest {
    
    public static final String RESOURCES_PREFIX = "src/test/resources";

    final static ObjectMapper objectMapper = new ObjectMapper();

    private static SantaMap santaMap;

    @BeforeAll
    static void load_map() throws IOException {
        santaMap = objectMapper.readValue(Paths.get( RESOURCES_PREFIX + "/map.json").toFile(), SantaMap.class);
    }

    @Test
    void success_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/success_route.json").toFile(), Route.class);
        Assertions.assertTrue(route.isValid(santaMap));
    }

    @Test
    void weight_control_success_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/weight_control_success_route.json").toFile(), Route.class);
        Assertions.assertTrue(route.isValid(santaMap));
    }

    @Test
    void volume_control_success_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/volume_control_success_route.json").toFile(), Route.class);
        Assertions.assertTrue(route.isValid(santaMap));
    }

    @Test
    void gifts_duplicate_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/gifts_duplicate_route.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void extra_gifts_taken() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/extra_gifts_taken.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void not_all_gifts_taken() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/not_all_gifts_taken.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void weight_and_volume_control_failed_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/weight_and_volume_control_failed_route.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void not_all_child_visited_failed_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/not_all_child_visited_failed_route.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void not_enough_gifts_in_try_fail_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/not_enough_gifts_in_try_fail_route.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @Test
    void duplicates_in_row_fail_route() throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/duplicates_in_row_fail_route.json").toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }

    @ParameterizedTest
    @ValueSource(strings = {"boundaries_max_x_fail_route.json", "boundaries_max_y_fail_route.json",
            "boundaries_min_x_fail_route.json", "boundaries_min_y_fail_route.json"})
    void boundaries_fail_route(final String filename) throws IOException {
        final Route route = objectMapper.readValue(Paths.get(RESOURCES_PREFIX + "/routes/" + filename).toFile(), Route.class);
        Assertions.assertFalse(route.isValid(santaMap));
    }
}
