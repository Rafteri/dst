package com.rafteri.dst.model;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
public record RouteResults(double time, double distance) {
    public static final RouteResults ERROR = new RouteResults(-1, -1);
}
