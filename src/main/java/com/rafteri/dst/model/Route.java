package com.rafteri.dst.model;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
@Slf4j
public record Route(String mapID, List<Point> moves,
                    Deque<List<Integer>> stackOfBags) {

    public boolean isValid(final SantaMap santaMap) {
        final List<Integer> giftIdsInBags = stackOfBags.stream()
                .flatMap(Collection::stream)
                .toList();
        Map<Integer, SantaMap.Gift> giftByIdMap = santaMap.gifts()
                .stream()
                .collect(Collectors.toMap(SantaMap.Gift::id, Function.identity()));

        return // gifts checks
                checkGiftDuplicates(giftIdsInBags)
                        && isExtraGiftsTaken(santaMap.gifts(), giftIdsInBags)
                        && isAllGiftsTaken(santaMap.gifts(), giftIdsInBags)
                        && weightAndVolumeControl(stackOfBags, giftByIdMap)
                        // moves
                        && isAllChildrenPresent(santaMap.children(), moves)
                        && isInBoundaries(moves)
                        && isNoDuplicatesInRow(moves)
                        // common
                        && enoughGiftsForChildrenInEachTry(santaMap.children(), stackOfBags, moves);
    }

    private boolean checkGiftDuplicates(final List<Integer> giftIdsInBags) {
        final List<Integer> duplicates = new ArrayList<>();
        final Set<Integer> uniques = new HashSet<>();

        for (Integer gId : giftIdsInBags) {
            if (!uniques.add(gId)) {
                duplicates.add(gId);
            }
        }
        if (!duplicates.isEmpty()) {
            log.error("There is duplicates for gifts with ids: {}", duplicates);
            return false;
        }
        return true;
    }

    private boolean isExtraGiftsTaken(List<SantaMap.Gift> gifts, List<Integer> giftIdsInBags) {
        final List<Integer> idsInBags = new ArrayList<>(giftIdsInBags);
        idsInBags.removeAll(gifts.stream().map(SantaMap.Gift::id).toList());
        if (!idsInBags.isEmpty()) {
            log.error("There is some extra gifts taken with ids: {}", idsInBags);
            return false;
        }
        return true;
    }

    private boolean isAllGiftsTaken(final List<SantaMap.Gift> gifts, final List<Integer> giftIdsInBags) {
        final List<Integer> allGiftIds = new ArrayList<>(gifts.stream().map(SantaMap.Gift::id).toList());
        allGiftIds.removeAll(giftIdsInBags);
        if (!allGiftIds.isEmpty()) {
            log.error("There is forgotten gifts with ids: {}", allGiftIds);
            return false;
        }
        return true;
    }

    private boolean weightAndVolumeControl(final Deque<List<Integer>> stackOfBags, final Map<Integer, SantaMap.Gift> giftByIdMap) {
        final List<List<Integer>> wrongBags = new ArrayList<>();
        stackOfBags.forEach(bag -> {
            int volume = 0;
            int weight = 0;
            for (Integer giftId : bag) {
                final SantaMap.Gift gift = giftByIdMap.get(giftId);
                volume += gift.volume();
                weight += gift.weight();
            }
            if (volume > SantaMap.MAX_VOLUME && weight > SantaMap.MAX_WEIGHT) {
                log.error("Too much weight {} and volume {} for bag {}", weight, volume, bag);
                wrongBags.add(bag);
            }
        });
        return wrongBags.isEmpty();
    }

    private boolean isAllChildrenPresent(final List<Point> children, final List<Point> moves) {
        final Set<String> allMovesCoords = moves.stream()
                .map(m -> m.x() + ";" + m.y())
                .collect(Collectors.toSet());
        final Set<String> allChildCoords = children.stream()
                .map(c -> c.x() + ";" + c.y())
                .collect(Collectors.toSet());
        // removing non child coords from path
        allMovesCoords.retainAll(allChildCoords);
        allChildCoords.removeAll(allMovesCoords);
        if (!allChildCoords.isEmpty()) {
            log.error("Missing in moves children with coords {}", allChildCoords);
            return false;
        }
        return true;
    }

    private boolean isInBoundaries(final List<Point> moves) {
        final DoubleSummaryStatistics xStats = moves.stream().mapToDouble(Point::x).summaryStatistics();
        final DoubleSummaryStatistics yStats = moves.stream().mapToDouble(Point::y).summaryStatistics();

        if (xStats.getMin() < SantaMap.MIX_X || xStats.getMax() > SantaMap.MAX_X
                || yStats.getMin() < SantaMap.MIX_Y || yStats.getMax() > SantaMap.MAX_Y) {
            log.error("Alarm! You escaped boundaries minX - {}, maxX - {}, minY - {}, maxY - {}",
                    xStats.getMin(), xStats.getMax(), yStats.getMin(), yStats.getMax());
            return false;
        }
        return true;
    }

    private boolean isNoDuplicatesInRow(List<Point> moves) {
        final Iterator<Point> movesIterator = moves.iterator();
        Point prevPoint = movesIterator.next();
        while (movesIterator.hasNext()) {
            final Point currMove = movesIterator.next();
            if (currMove.equals(prevPoint)) {
                log.error("Duplicated point in move ({};{})", currMove.x(), currMove.y());
                return false;
            }
            prevPoint = currMove;
        }

        return true;
    }

    private boolean enoughGiftsForChildrenInEachTry(final List<Point> children,
                                                    final Deque<List<Integer>> bags, final List<Point> moves) {
        final List<String> allMovesCoords = moves.stream()
                .map(m -> (int) m.x() + ";" + (int) m.y())
                .toList();
        final Set<String> allChildCoords = children.stream()
                .map(c -> (int) c.x() + ";" + (int) c.y())
                .collect(Collectors.toSet());
        final Iterator<List<Integer>> bagsIterator = bags.descendingIterator();
        List<Integer> currentBag = bagsIterator.next();
        int childsInMove = 0;
        boolean isCorrect = true;

        List<String> path = new ArrayList<>();
        for (String moveCoords : allMovesCoords) {
            path.add(moveCoords);
            if (allChildCoords.contains(moveCoords)) {
                allChildCoords.remove(moveCoords);
                childsInMove++;
            }
            if (moveCoords.equals("0;0")) {
                if (childsInMove != currentBag.size()) {
                    log.error("Child count {} doesn't match with bag size {}\nBag: {}\nPath: {}",
                            childsInMove, currentBag.size(), currentBag, path);
                    isCorrect = false;
                }

                currentBag = bagsIterator.next();
                childsInMove = 0;
                path.clear();
            }
        }
        if (!path.isEmpty() && childsInMove != currentBag.size()) {
            log.error("Child count {} doesn't match with bag size {}\nBag: {}\nPath: {}",
                    childsInMove, currentBag.size(), currentBag, path);
            isCorrect = false;
        }

        return isCorrect;
    }
}
