package com.rafteri.dst.model;

import java.util.List;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
public record SantaMap(List<Gift> gifts,
                       List<SnowArea> snowAreas,
                       List<Point> children) {
    public static final int MAX_Y = 10000;
    public static final int MAX_X = 10000;
    public static final int MIX_Y = 0;
    public static final int MIX_X = 0;
    public static final int MAX_WEIGHT = 200;
    public static final int MAX_VOLUME = 100;


    public record Gift(int id, int weight, int volume) {
    }

    public record SnowArea(int x, int y, int r) {
    }
}
