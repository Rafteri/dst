package com.rafteri.dst.model;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
public record Point(float x, float y) {
    public static final Point HOME = new Point(0, 0);

    public double distanceTo(Point to) {
        return Math.sqrt((to.y() - y()) * (to.y() - y()) + (to.x() - x()) * (to.x() - x()));
    }

    public Point minByX(final Point point) {
        return x() < point.x ? this : point;
    }

    public Point maxByX(final Point point) {
        return x() < point.x ? point : this;
    }
}