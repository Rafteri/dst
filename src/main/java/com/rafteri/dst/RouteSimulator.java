package com.rafteri.dst;

import com.rafteri.dst.model.Point;
import com.rafteri.dst.model.Route;
import com.rafteri.dst.model.RouteResults;
import com.rafteri.dst.model.SantaMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
@Slf4j
@RequiredArgsConstructor
public class RouteSimulator {
    private final SantaMap santaMap;

    public static final int NORMAL_SPEED = 70;
    public static final int SNOWED_SPEED = 10;
    public static final float DISTANCE_COEFF = 1;

    public RouteResults simulate(final Route route) {
        if (!route.isValid(santaMap)) return RouteResults.ERROR;

        double timeAmount = 0;
        double distance = 0;

        final List<Point> children = new ArrayList<>(santaMap.children());
        final List<Point> moves = route.moves();
        final Iterator<List<Integer>> bags = route.stackOfBags().descendingIterator();

        Point prevPoint = Point.HOME;
        List<Integer> currentBag = bags.next();
        Iterator<Integer> currentBagIterator = currentBag.iterator();

        for (Point move : moves) {
            timeAmount += calculateMoveTime(prevPoint, move, santaMap.snowAreas());
            distance += DISTANCE_COEFF * prevPoint.distanceTo(move);
            if (children.remove(move)) {
                log.info("Gifted to child ({};{}) gift {}", move.x(), move.y(), currentBagIterator.next());
            }
            if (move.equals(Point.HOME)) {
                log.info("Returned to home for another bag");

                if (currentBagIterator.hasNext()) {
                    final List<Integer> remainingGifts = new ArrayList<>();
                    currentBagIterator.forEachRemaining(remainingGifts::add);
                    log.error("Returned home with remaining gifts {}", remainingGifts);
                    return RouteResults.ERROR;
                }

                currentBag = bags.next();
                currentBagIterator = currentBag.iterator();
            }
            prevPoint = move;
        }

        if (!children.isEmpty()) {
            log.error("Children - {} - left without gifts", children);
        }

        return new RouteResults(timeAmount, distance);
    }

    private double calculateMoveTime(Point prevPoint, Point move, List<SantaMap.SnowArea> snowAreas) {
        final double distance = prevPoint.distanceTo(move);

        final double chordsDistance = snowAreas.stream()
                .mapToDouble(area -> calculateChordLength(area.x(), area.y(), area.r(), prevPoint, move))
                .sum();

        return DISTANCE_COEFF * (chordsDistance / SNOWED_SPEED + (distance - chordsDistance) / NORMAL_SPEED);
    }

    private static double calculateChordLength(int x0, int y0, int r, Point p1, Point p2) {
        final float k =  (p2.y() - p1.y()) / (p2.x() - p1.x());
        final float b = p1.y() - k * p1.x();

        final float A = 1 + k * k;
        final float B = -2 * (x0 + k * (y0 - b));
        final float C = x0 * x0 + (y0 - b) * (y0 - b) - r * r;

        final float disc = B * B - 4 * A * C;

        if (disc <= 0) return 0;

        final float x01 = (float) (-B + Math.sqrt(disc)) / (2 * A);
        final float x02 = (float) (-B - Math.sqrt(disc)) / (2 * A);

        final Point p01 = new Point(x01, k * x01 + b);
        final Point p02 = new Point(x02, k * x02 + b);

        final Point intersectMin = p01.minByX(p02);
        final Point intersectMax = p01.maxByX(p02);

        final Point pMin = p1.minByX(p2);
        final Point pMax = p1.maxByX(p2);

        if (intersectMin.x() >= pMin.x() && intersectMax.x() <= pMax.x()) {
            return intersectMin.distanceTo(intersectMax);
        } else if (intersectMin.x() >= pMin.x() && intersectMin.x() <= pMax.x() && intersectMax.x() >= pMax.x()) {
            return intersectMin.distanceTo(pMax);
        } else if (intersectMax.x() >= pMin.x() && intersectMax.x() <= pMax.x() && intersectMin.x() <= pMin.x()) {
            return intersectMin.distanceTo(pMax);
        } else if (intersectMin.x() <= pMin.x() && intersectMax.x() >= pMax.x()) {
            return pMin.distanceTo(pMax);
        } else {
            return 0;
        }
    }
}
