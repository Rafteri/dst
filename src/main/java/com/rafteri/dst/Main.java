package com.rafteri.dst;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rafteri.dst.model.Route;
import com.rafteri.dst.model.RouteResults;
import com.rafteri.dst.model.SantaMap;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * @author Dinar Rafikov
 * din.rafikov@gmail.com
 */
@Slf4j
public class Main {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws IOException {
        final SantaMap santaMap = objectMapper.readValue(Paths.get( "map.json").toFile(), SantaMap.class);
        final Route route = objectMapper.readValue(Paths.get( "route.json").toFile(), Route.class);

        final RouteSimulator routeSimulator = new RouteSimulator(santaMap);
        final RouteResults results = routeSimulator.simulate(route);

        log.info("Distance - {}, time - {}", results.distance(), results.time());
    }
}
